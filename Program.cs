﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collection;

namespace CompetEF
{
    class Program
    {
        static void Main(string[] args)
        {
            StartMethod();
        }

        public static void StartMethod()
        {
            Console.WriteLine("Type Add to add competitor, write Get to see who's already been added, or X to eXit");
            string input = Console.ReadLine();
            switch (input.ToLower())
            {
                case "add":
                    Console.WriteLine("Name:");
                    string name = Console.ReadLine();
                    Console.WriteLine("Age:");
                    string age = Console.ReadLine();
                    Console.WriteLine("Nationality:");
                    string nationality = Console.ReadLine();
                    Console.WriteLine("Role:");
                    string role = Console.ReadLine();
                    Console.WriteLine("Organisation:");
                    string organisation = Console.ReadLine();
                    Competitor comp = new Competitor(name, Int32.Parse(age), nationality, role, organisation);
                    using (CompetEFDbContext comtext = new CompetEFDbContext())
                    {
                        comtext.Competitors.Add(comp);
                        comtext.SaveChanges();
                    }
                    StartMethod();
                    break;
                case "get":
                    using (CompetEFDbContext comtext = new CompetEFDbContext())
                    {
                        List<Competitor> allCompetitors = comtext.Competitors.ToList();
                        foreach(Competitor compe in allCompetitors)
                        {
                            Console.WriteLine("Name: " + compe.Name + ", age: " + compe.Age + ", nationality: " + compe.Nationality
                                + ", role: " + compe.Role + ", organisation: " + compe.Organisation);
                        }
                    }
                    StartMethod();
                    break;
                case "x":
                    break;
                default:
                    Console.WriteLine("Invalid input, try again!");
                    StartMethod();
                    break;
            }
        }
    }
}
