# Competitor EF
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
> Entity Framework Competitor Assignment
## Table of Contents
- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)
## Run
```
Visual Studio Run
```
## Components
Console App that can add Competitors to SQL Db or fetch all and print, .dll in Lib folder
## Maintainers
[Philip Aubert] (@pauberexperis)](https://gitlab.com/pauberexperis)
## Contributing
PRs accepted.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.
## License
Experis © 2020 Noroff Accelerate AS