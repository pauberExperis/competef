﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Collection;

namespace CompetEF
{
    class CompetEFDbContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=PC7364\SQLEXPRESS;Database=Comdb;Trusted_Connection=true;MultipleActiveResultSets=true");
        }
        public DbSet<Competitor> Competitors { get; set; }
    }
}
